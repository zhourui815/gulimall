package site.zhourui.gulimall.order;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.lang.Nullable;
import site.zhourui.gulimall.order.entity.OrderReturnReasonEntity;

import java.util.Date;
import java.util.Map;

@Slf4j
@SpringBootTest
class GulimallOrderApplicationTests {
    @Autowired
    AmqpAdmin amqpAdmin;

    /**
     * 创建交换机
     */
    @Test
    void createExchange() {
        // public DirectExchange(String name,     交换机名称
        // boolean durable,                        是否持久化
        // boolean autoDelete,                      是否自动删除
        // Map<String, Object> arguments)           参数
        DirectExchange directExchange = new DirectExchange("test_exchange",false,false,null);
        amqpAdmin.declareExchange(directExchange);
    }

    @Test
    void createQueue() {
//        public Queue(String name,   队列名称
//        boolean durable,            是否持久化
//        boolean exclusive,            是否是排他队列(只能被一个consumer的连接占用)
//        boolean autoDelete,          是否自动删除
//        @Nullable Map<String, Object> arguments)  参数
        Queue queue = new Queue("test_queue",true,false,false);
        amqpAdmin.declareQueue(queue);
    }

    @Test
    void createBinding() {
//	public Binding(String destination                      【目的地，队列name或 交换机name(如果作为路由的话)】
//                Binding.DestinationType destinationType,  【目的地类型 queue还是exchange(路由)】
//                String exchange,                          【交换机】
//                String routingKey,                        【路由键】
//                @Nullable Map<String, Object> arguments)  【自定义参数】
        Binding binding = new Binding("test_queue", Binding.DestinationType.QUEUE,"test_exchange","test.binding",null);
        amqpAdmin.declareBinding(binding);
    }

    @Autowired
    RabbitTemplate rabbitTemplate;

    @Test
    void sendMessage() {
//        public void convertAndSend(String exchange,     交换机
//                String routingKey,                      路由键
//                Object message,                         发送的消息
//                MessagePostProcessor messagePostProcessor) 消息序列化器
        OrderReturnReasonEntity orderReturnReasonEntity = new OrderReturnReasonEntity();
        orderReturnReasonEntity.setId(1L);
        orderReturnReasonEntity.setName("sendMessageTest2");
        orderReturnReasonEntity.setCreateTime(new Date());
        rabbitTemplate.convertAndSend("test_exchange","test.binding",orderReturnReasonEntity);
        log.info("消息发送完成");
    }

//    @RabbitListener(queues = "test_queue")
//    void receiveMessage(Object msg) {
//        log.info("收到消息内容:"+msg+"==>类型:"+msg.getClass());
//    }

//    @RabbitListener(queues = "test_queue")
//    void receiveMessage2(Message msg,OrderReturnReasonEntity orderReturnReasonEntity) {
//        byte[] body = msg.getBody();
//        MessageProperties messageProperties = msg.getMessageProperties();
//        log.info("收到消息内容:"+msg+"==>内容"+orderReturnReasonEntity);
//    }
}
