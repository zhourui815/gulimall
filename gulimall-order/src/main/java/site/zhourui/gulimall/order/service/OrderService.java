package site.zhourui.gulimall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import site.zhourui.common.to.mq.SeckillOrderTo;
import site.zhourui.common.utils.PageUtils;
import site.zhourui.gulimall.order.config.AlipayTemplate;
import site.zhourui.gulimall.order.entity.OrderEntity;
import site.zhourui.gulimall.order.vo.OrderConfirmVo;
import site.zhourui.gulimall.order.vo.OrderSubmitVo;
import site.zhourui.gulimall.order.vo.PayAsyncVo;
import site.zhourui.gulimall.order.vo.SubmitOrderResponseVo;

import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * 订单
 *
 * @author zr
 * @email 2437264464@qq.com
 * @date 2021-09-28 15:42:46
 */
public interface OrderService extends IService<OrderEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 订单确认页返回需要用的数据
     * @return
     */
    OrderConfirmVo confirmOrder() throws ExecutionException, InterruptedException;
    /**
     * 创建订单
     * @param vo
     * @return
     */
    SubmitOrderResponseVo submitOrder(OrderSubmitVo vo);
    /**
     * 按照订单号获取订单信息
     * @param orderSn
     * @return
     */
    OrderEntity getOrderByOrderSn(String orderSn);
    /**
     * 关闭订单
     * @param orderEntity
     */
    void closeOrder(OrderEntity orderEntity);

    /**
     * 获取当前订单的支付信息
     * @param orderSn
     * @return
     */
    AlipayTemplate.PayVo getOrderPay(String orderSn);
    /**
     * 查询当前用户所有订单数据
     * @param params
     * @return
     */
    PageUtils queryPageWithItem(Map<String, Object> params);

    /**
     *支付宝异步通知处理订单数据
     * @param asyncVo
     * @return
     */
    String handlePayResult(PayAsyncVo asyncVo);
    /**
     * 创建秒杀单
     * @param orderTo
     */
    void createSeckillOrder(SeckillOrderTo orderTo);
}

