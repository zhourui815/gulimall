package site.zhourui.gulimall.order.constant;

/**
 * @author zr
 * @date 2021/12/23 22:05
 */
public class OrderConstant {
    public static final String USER_ORDER_TOKEN_PREFIX = "order:token";

}
