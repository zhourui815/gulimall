package site.zhourui.gulimall.order.controller;

import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import site.zhourui.common.utils.PageUtils;
import site.zhourui.common.utils.R;
import site.zhourui.gulimall.order.entity.OrderEntity;
import site.zhourui.gulimall.order.entity.OrderReturnReasonEntity;

import java.util.Date;
import java.util.Map;
import java.util.UUID;

/**
 * @author zr
 * @date 2021/12/15 14:12
 */
@RestController
public class TestSendMessageController {
    @Autowired
    RabbitTemplate rabbitTemplate;

    /**
     * 列表
     */
    @RequestMapping("/sendMessage")
    public R sendMany(@RequestParam("num") Integer num){
        for (int i = 0; i <num; i++) {
            OrderReturnReasonEntity orderReturnReasonEntity = new OrderReturnReasonEntity();
            orderReturnReasonEntity.setId(1L);
            orderReturnReasonEntity.setName("消息---"+i);
            orderReturnReasonEntity.setCreateTime(new Date());
            rabbitTemplate.convertAndSend("test_exchange","test.binding",orderReturnReasonEntity,new CorrelationData(UUID.randomUUID().toString()));
        }
        return R.ok();
    }


//    /**
//     * 列表
//     */
//    @RequestMapping("/sendMessage")
//    public R sendMany(@RequestParam("num") Integer num){
//        for (int i = 0; i <num; i++) {
//            if (i%2==0){
//                OrderReturnReasonEntity orderReturnReasonEntity = new OrderReturnReasonEntity();
//                orderReturnReasonEntity.setId(1L);
//                orderReturnReasonEntity.setName("orderReturnReasonEntity消息---" + i);
//                orderReturnReasonEntity.setCreateTime(new Date());
//                rabbitTemplate.convertAndSend("test_exchange", "test.binding", orderReturnReasonEntity);
//            }else {
//                OrderEntity orderEntity = new OrderEntity();
//                orderEntity.setOrderSn(UUID.randomUUID().toString()+"orderEntity消息---"+i);
//                rabbitTemplate.convertAndSend("test_exchange", "test.binding", orderEntity);
//            }
//        }
//
//        return R.ok();
//    }
}
