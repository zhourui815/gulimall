package site.zhourui.gulimall.order.constant;

/**
 * @author zr
 * @date 2021/12/31 17:38
 */
public class PayConstant {

    public static final Integer ALIPAY = 1;

    public static final Integer WXPAY = 2;

}
