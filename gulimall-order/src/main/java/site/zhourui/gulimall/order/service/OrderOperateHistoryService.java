package site.zhourui.gulimall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import site.zhourui.common.utils.PageUtils;
import site.zhourui.gulimall.order.entity.OrderOperateHistoryEntity;

import java.util.Map;

/**
 * 订单操作历史记录
 *
 * @author zr
 * @email 2437264464@qq.com
 * @date 2021-09-28 15:42:46
 */
public interface OrderOperateHistoryService extends IService<OrderOperateHistoryEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

