package site.zhourui.gulimall.order.vo;

import lombok.Data;
import site.zhourui.gulimall.order.entity.OrderEntity;

/**
 * 提交订单返回结果
 * @author zr
 * @date 2021/12/23 22:17
 */
@Data
public class SubmitOrderResponseVo {
    private OrderEntity order;

    /** 错误状态码 0成功**/
    private Integer code;
}
