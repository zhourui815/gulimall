package site.zhourui.gulimall.coupon.dao;

import site.zhourui.gulimall.coupon.entity.SeckillPromotionEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 秒杀活动
 * 
 * @author zr
 * @email 2437264464@qq.com
 * @date 2021-09-28 15:30:16
 */
@Mapper
public interface SeckillPromotionDao extends BaseMapper<SeckillPromotionEntity> {
	
}
