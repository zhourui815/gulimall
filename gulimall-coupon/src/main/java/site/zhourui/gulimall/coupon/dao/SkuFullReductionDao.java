package site.zhourui.gulimall.coupon.dao;

import site.zhourui.gulimall.coupon.entity.SkuFullReductionEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品满减信息
 * 
 * @author zr
 * @email 2437264464@qq.com
 * @date 2021-09-28 15:30:16
 */
@Mapper
public interface SkuFullReductionDao extends BaseMapper<SkuFullReductionEntity> {
	
}
