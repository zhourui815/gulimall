package site.zhourui.gulimall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import site.zhourui.common.utils.PageUtils;
import site.zhourui.gulimall.coupon.entity.SeckillSessionEntity;

import java.util.List;
import java.util.Map;

/**
 * 秒杀活动场次
 *
 * @author zr
 * @email 2437264464@qq.com
 * @date 2021-09-28 15:30:16
 */
public interface SeckillSessionService extends IService<SeckillSessionEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<SeckillSessionEntity> getLates3DaySession();

}

