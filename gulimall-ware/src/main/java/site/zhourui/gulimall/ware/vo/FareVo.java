package site.zhourui.gulimall.ware.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author zr
 * @date 2021/12/23 18:24
 */
@Data
public class FareVo {

    private MemberAddressVo address;

    private BigDecimal fare;

}
