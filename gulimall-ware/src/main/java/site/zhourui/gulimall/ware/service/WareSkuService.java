package site.zhourui.gulimall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import site.zhourui.common.to.mq.OrderTo;
import site.zhourui.common.to.mq.StockLockedTo;
import site.zhourui.common.utils.PageUtils;
import site.zhourui.gulimall.ware.entity.WareSkuEntity;
import site.zhourui.gulimall.ware.vo.SkuHasStockVo;
import site.zhourui.gulimall.ware.vo.WareSkuLockVo;

import java.util.List;
import java.util.Map;

/**
 * 商品库存
 *
 * @author zr
 * @email 2437264464@qq.com
 * @date 2021-09-28 15:47:50
 */
public interface WareSkuService extends IService<WareSkuEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void addStock(Long skuId, Long wareId, Integer skuNum);
    /**
     * 判断是否有库存
     */
    List<SkuHasStockVo> getSkusHasStock(List<Long> skuIds);
    /**
     * 锁定库存
     */
    boolean orderLockStock(WareSkuLockVo vo);

    /**
     * 解锁库存
     * @param to
     */
    void unlockStock(StockLockedTo to);

    /**
     * 解锁订单
     */
    void unlockStock(OrderTo orderTo);
}

