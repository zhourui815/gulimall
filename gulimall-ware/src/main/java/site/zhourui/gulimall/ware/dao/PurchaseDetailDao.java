package site.zhourui.gulimall.ware.dao;

import site.zhourui.gulimall.ware.entity.PurchaseDetailEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author zr
 * @email 2437264464@qq.com
 * @date 2021-09-28 15:47:50
 */
@Mapper
public interface PurchaseDetailDao extends BaseMapper<PurchaseDetailEntity> {
	
}
