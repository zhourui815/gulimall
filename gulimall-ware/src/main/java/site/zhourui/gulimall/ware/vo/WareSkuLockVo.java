package site.zhourui.gulimall.ware.vo;

import lombok.Data;

import java.util.List;

/**
 * 锁定库存的vo
 * @author zr
 * @date 2021/12/24 10:22
 */
@Data
public class WareSkuLockVo {

    private String orderSn;

    /** 需要锁住的所有库存信息 **/
    private List<OrderItemVo> locks;

}
