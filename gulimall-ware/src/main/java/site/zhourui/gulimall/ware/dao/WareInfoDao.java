package site.zhourui.gulimall.ware.dao;

import site.zhourui.gulimall.ware.entity.WareInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 仓库信息
 * 
 * @author zr
 * @email 2437264464@qq.com
 * @date 2021-09-28 15:47:50
 */
@Mapper
public interface WareInfoDao extends BaseMapper<WareInfoEntity> {
	
}
