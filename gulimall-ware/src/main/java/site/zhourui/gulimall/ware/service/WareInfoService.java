package site.zhourui.gulimall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import site.zhourui.common.utils.PageUtils;
import site.zhourui.gulimall.ware.entity.WareInfoEntity;
import site.zhourui.gulimall.ware.vo.FareVo;

import java.util.Map;

/**
 * 仓库信息
 *
 * @author zr
 * @email 2437264464@qq.com
 * @date 2021-09-28 15:47:50
 */
public interface WareInfoService extends IService<WareInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);
    /**
     * 获取运费和收货地址信息
     * @param addrId
     * @return
     */
    FareVo getFare(Long addrId);
}

