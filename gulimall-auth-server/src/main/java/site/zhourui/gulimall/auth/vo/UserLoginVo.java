package site.zhourui.gulimall.auth.vo;

import lombok.Data;

/**
 * @author zr
 * @date 2021/11/30 13:51
 */
@Data
public class UserLoginVo {
    private String loginacct;
    private String password;
}
