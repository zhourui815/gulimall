package site.zhourui.gulimall.auth.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import site.zhourui.common.utils.R;
import site.zhourui.gulimall.auth.vo.GiteeSocialUser;

import site.zhourui.gulimall.auth.vo.UserLoginVo;
import site.zhourui.gulimall.auth.vo.UserRegisterVo;

/**
 * @author zr
 * @date 2021/11/30 11:12
 */
@FeignClient("gulimall-member")
public interface MemberFeignService {
    @PostMapping(value = "/member/member/register")
    R register(@RequestBody UserRegisterVo vo);

    @PostMapping(value = "/member/member/login")
    R login(@RequestBody UserLoginVo vo);

    @PostMapping(value = "/member/member/oauth2/login")
    R oauthLogin(@RequestBody GiteeSocialUser socialUser) throws Exception;
}
