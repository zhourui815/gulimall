package site.zhourui.gilimall.search.vo;

import lombok.Data;

/**
 * @author zr
 * @date 2021/11/22 22:06
 */
@Data
public class BrandVo {
    private Long brandId;
    private String name;
}
