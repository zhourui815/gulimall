package site.zhourui.gilimall.search.service;

import site.zhourui.gilimall.search.vo.SearchParam;
import site.zhourui.gilimall.search.vo.SearchResult;

/**
 * @author zr
 * @date 2021/11/17 18:06
 */
public interface MallSearchService {
    SearchResult search(SearchParam param);
}
