package site.zhourui.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import site.zhourui.common.utils.PageUtils;
import site.zhourui.gulimall.product.entity.CategoryEntity;
import site.zhourui.gulimall.product.vo.Catalog2Vo;

import java.util.List;
import java.util.Map;

/**
 * 商品三级分类
 *
 * @author zr
 * @email 2437264464@qq.com
 * @date 2021-09-28 15:45:02
 */
public interface CategoryService extends IService<CategoryEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<CategoryEntity> listWithTree();
    /**
     * 找到catelogId的完整路径；
     * [父/子/孙]
     * @param catelogId
     * @return
     */
    Long[] findCatelogPath(Long catelogId);
    void updateCascade(CategoryEntity category);

    List<CategoryEntity> getLevel1Categorys();

    Map<String, List<Catalog2Vo>> getCatalogJson();
}

