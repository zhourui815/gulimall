package site.zhourui.gulimall.product.dao;

import site.zhourui.gulimall.product.entity.CategoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品三级分类
 * 
 * @author zr
 * @email 2437264464@qq.com
 * @date 2021-09-28 15:45:02
 */
@Mapper
public interface CategoryDao extends BaseMapper<CategoryEntity> {
	
}
