package site.zhourui.gulimall.product.fallback;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import site.zhourui.common.exception.BizCodeEnume;
import site.zhourui.common.utils.R;
import site.zhourui.gulimall.product.feign.SeckillFeignService;

/**
 * 熔断方法的具体实现，也可以是降级方法的具体实现
 * @author zr
 * @date 2022/1/11 11:50
 */
@Component
@Slf4j
public class SeckillFeignServiceFallBack implements SeckillFeignService {
    @Override
    public R getSkuSeckilInfo(Long skuId) {
        log.info("熔断方法调用...getSkuSeckilInfo");
        return R.error(BizCodeEnume.TO_MANY_REQUEST.getCode(),BizCodeEnume.TO_MANY_REQUEST.getMsg());
    }
}
