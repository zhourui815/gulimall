package site.zhourui.gulimall.product.feign;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import site.zhourui.common.to.es.SkuEsModel;
import site.zhourui.common.utils.R;

import java.util.List;

@FeignClient("gulimall-search")
public interface SearchFeignService {
    // 上架商品
    @PostMapping("/search/save/product")
    R productStatusUp(@RequestBody List<SkuEsModel> skuEsModels);
}
