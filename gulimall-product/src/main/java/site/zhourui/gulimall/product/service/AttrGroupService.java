package site.zhourui.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import site.zhourui.common.utils.PageUtils;
import site.zhourui.gulimall.product.entity.AttrGroupEntity;
import site.zhourui.gulimall.product.vo.AttrGroupWithAttrsVo;
import site.zhourui.gulimall.product.vo.SpuItemAttrGroupVo;

import java.util.List;
import java.util.Map;

/**
 * 属性分组
 *
 * @author zr
 * @email 2437264464@qq.com
 * @date 2021-09-28 15:45:02
 */
public interface AttrGroupService extends IService<AttrGroupEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 根据catelogId查询属性分组
     * @param params
     * @param catelogId
     * @return
     */
    PageUtils queryPage(Map<String, Object> params, Long catelogId);

    List<AttrGroupWithAttrsVo> getAttrGroupWithAttrsByCatelogId(Long catelogId);

    List<SpuItemAttrGroupVo> getAttrGroupWithAttrsBySpuId(Long spuId, Long catalogId);
}

