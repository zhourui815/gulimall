package site.zhourui.gulimall.product.vo;

import lombok.Data;

/**
 * @author zr
 * @date 2021/11/25 22:47
 */
@Data
public class AttrValueWithSkuIdVo {
    private String attrValue;
    private String skuIds;
}
