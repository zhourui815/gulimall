package site.zhourui.gulimall.seckill.scheduled;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author zr
 * @date 2022/1/6 14:29
 */
//@Slf4j
//@Component
//@EnableScheduling
//@EnableAsync
//public class HelloScheduled {
//    @Async
//    @Scheduled(cron = "* * * * * ? ")
//    public void hello(){
//        try {
//            Thread.sleep(3000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        log.info("hello world");
//    }
//}
