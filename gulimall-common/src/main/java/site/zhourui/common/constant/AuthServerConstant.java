package site.zhourui.common.constant;

/**
 * @author zr
 * @date 2021/11/29 22:47
 */
public class AuthServerConstant {
    public static final String SMS_CODE_CACHE_PREFIX = "sms:code:";

    public static final String LOGIN_USER = "loginUser";
}
