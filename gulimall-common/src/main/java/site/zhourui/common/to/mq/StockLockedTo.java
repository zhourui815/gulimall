package site.zhourui.common.to.mq;

/**
 *  锁定库存成功，往延时队列存入 工作单to 对象
 *  wms_ware_order_task
 * @author zr
 * @date 2021/12/29 15:07
 */

import lombok.Data;

/**

 */
@Data
public class StockLockedTo {

    /** 库存工作单的id **/
    private Long id;

    /** 库存单详情 wms_ware_order_task_detail**/
    private StockDetailTo detailTo;
}
