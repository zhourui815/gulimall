package site.zhourui.common.to;

/**
 * @author zr
 * @date 2021/12/13 15:18
 */

import lombok.Data;
import lombok.ToString;

/**
 * 用户信息
 **/
@ToString
@Data
public class UserInfoTo {
    private Long userId;
    private String userKey; // 关联购物车
    private boolean tempUser = false;// 客户端是否需要存储cookie：user-key
}
