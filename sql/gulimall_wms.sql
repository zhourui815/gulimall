/*
 Navicat Premium Data Transfer

 Source Server         : gulimall1
 Source Server Type    : MySQL
 Source Server Version : 50735
 Source Host           : 10.0.0.41:3306
 Source Schema         : gulimall_wms

 Target Server Type    : MySQL
 Target Server Version : 50735
 File Encoding         : 65001

 Date: 25/01/2022 17:42:55
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for undo_log
-- ----------------------------
DROP TABLE IF EXISTS `undo_log`;
CREATE TABLE `undo_log`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `branch_id` bigint(20) NOT NULL,
  `xid` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `context` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `rollback_info` longblob NOT NULL,
  `log_status` int(11) NOT NULL,
  `log_created` datetime NOT NULL,
  `log_modified` datetime NOT NULL,
  `ext` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `ux_undo_log`(`xid`, `branch_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of undo_log
-- ----------------------------

-- ----------------------------
-- Table structure for wms_purchase
-- ----------------------------
DROP TABLE IF EXISTS `wms_purchase`;
CREATE TABLE `wms_purchase`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `assignee_id` bigint(20) NULL DEFAULT NULL,
  `assignee_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `phone` char(13) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `priority` int(4) NULL DEFAULT NULL,
  `status` int(4) NULL DEFAULT NULL,
  `ware_id` bigint(20) NULL DEFAULT NULL,
  `amount` decimal(18, 4) NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '采购信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wms_purchase
-- ----------------------------
INSERT INTO `wms_purchase` VALUES (1, 1, 'admin', '13612345678', 0, 3, NULL, NULL, NULL, '2021-10-18 10:08:56');
INSERT INTO `wms_purchase` VALUES (2, 2, 'leifengyang', '12345678912', NULL, 1, NULL, NULL, '2021-10-18 16:42:01', '2021-10-18 16:42:01');

-- ----------------------------
-- Table structure for wms_purchase_detail
-- ----------------------------
DROP TABLE IF EXISTS `wms_purchase_detail`;
CREATE TABLE `wms_purchase_detail`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `purchase_id` bigint(20) NULL DEFAULT NULL COMMENT '采购单id',
  `sku_id` bigint(20) NULL DEFAULT NULL COMMENT '采购商品id',
  `sku_num` int(11) NULL DEFAULT NULL COMMENT '采购数量',
  `sku_price` decimal(18, 4) NULL DEFAULT NULL COMMENT '采购金额',
  `ware_id` bigint(20) NULL DEFAULT NULL COMMENT '仓库id',
  `status` int(11) NULL DEFAULT NULL COMMENT '状态[0新建，1已分配，2正在采购，3已完成，4采购失败]',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wms_purchase_detail
-- ----------------------------
INSERT INTO `wms_purchase_detail` VALUES (1, 2, 1, 100, NULL, 2, 3);
INSERT INTO `wms_purchase_detail` VALUES (2, 1, 2, 1, NULL, 3, 2);

-- ----------------------------
-- Table structure for wms_ware_info
-- ----------------------------
DROP TABLE IF EXISTS `wms_ware_info`;
CREATE TABLE `wms_ware_info`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '仓库名',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '仓库地址',
  `areacode` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '区域编码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '仓库信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wms_ware_info
-- ----------------------------
INSERT INTO `wms_ware_info` VALUES (2, '一号仓库', '北京', '001');
INSERT INTO `wms_ware_info` VALUES (3, '二号仓库', '上海', '002');
INSERT INTO `wms_ware_info` VALUES (4, '三号仓库', '广州', '003');

-- ----------------------------
-- Table structure for wms_ware_order_task
-- ----------------------------
DROP TABLE IF EXISTS `wms_ware_order_task`;
CREATE TABLE `wms_ware_order_task`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `order_id` bigint(20) NULL DEFAULT NULL COMMENT 'order_id',
  `order_sn` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'order_sn',
  `consignee` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '收货人',
  `consignee_tel` char(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '收货人电话',
  `delivery_address` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '配送地址',
  `order_comment` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '订单备注',
  `payment_way` tinyint(1) NULL DEFAULT NULL COMMENT '付款方式【 1:在线付款 2:货到付款】',
  `task_status` tinyint(2) NULL DEFAULT NULL COMMENT '任务状态',
  `order_body` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '订单描述',
  `tracking_no` char(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '物流单号',
  `create_time` datetime NULL DEFAULT NULL COMMENT 'create_time',
  `ware_id` bigint(20) NULL DEFAULT NULL COMMENT '仓库id',
  `task_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '工作单备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 62 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '库存工作单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wms_ware_order_task
-- ----------------------------
INSERT INTO `wms_ware_order_task` VALUES (30, NULL, '202112301109489701476390034868338690', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-12-30 03:09:49', NULL, NULL);
INSERT INTO `wms_ware_order_task` VALUES (31, NULL, '202112301118064111476392121282920449', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-12-30 03:18:07', NULL, NULL);
INSERT INTO `wms_ware_order_task` VALUES (32, NULL, '202112311103014391476750713429254145', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-12-31 03:03:02', NULL, NULL);
INSERT INTO `wms_ware_order_task` VALUES (33, NULL, '202112311137311551476759394438963202', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-12-31 03:37:31', NULL, NULL);
INSERT INTO `wms_ware_order_task` VALUES (34, NULL, '202112311542269341476821033016107010', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-12-31 07:42:27', NULL, NULL);
INSERT INTO `wms_ware_order_task` VALUES (35, NULL, '202112311635212581476834347079180290', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-12-31 08:35:21', NULL, NULL);
INSERT INTO `wms_ware_order_task` VALUES (36, NULL, '202112311745402941476852043015544834', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-12-31 09:45:41', NULL, NULL);
INSERT INTO `wms_ware_order_task` VALUES (37, NULL, '202112311749003711476852882199298049', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-12-31 09:49:01', NULL, NULL);
INSERT INTO `wms_ware_order_task` VALUES (38, NULL, '202112311817271761476860041054175234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-12-31 10:17:27', NULL, NULL);
INSERT INTO `wms_ware_order_task` VALUES (39, NULL, '202112311820202271476860766886846465', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-12-31 10:20:20', NULL, NULL);
INSERT INTO `wms_ware_order_task` VALUES (40, NULL, '202112311828244511476862797856923650', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-12-31 10:28:25', NULL, NULL);
INSERT INTO `wms_ware_order_task` VALUES (41, NULL, '202201021031251201477467535430504450', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-02 02:31:25', NULL, NULL);
INSERT INTO `wms_ware_order_task` VALUES (42, NULL, '202201021048437801477471891869519873', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-02 02:48:44', NULL, NULL);
INSERT INTO `wms_ware_order_task` VALUES (43, NULL, '202201021057200701477474057355124737', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-02 02:57:20', NULL, NULL);
INSERT INTO `wms_ware_order_task` VALUES (44, NULL, '202201021058510341477474438873210882', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-02 02:58:51', NULL, NULL);
INSERT INTO `wms_ware_order_task` VALUES (45, NULL, '202201021100314351477474859985526786', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-02 03:00:32', NULL, NULL);
INSERT INTO `wms_ware_order_task` VALUES (46, NULL, '202201021115572561477478743168638978', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-02 03:15:57', NULL, NULL);
INSERT INTO `wms_ware_order_task` VALUES (49, NULL, '202201101807116501480481338002780161', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-10 18:07:12', NULL, NULL);
INSERT INTO `wms_ware_order_task` VALUES (50, NULL, '202201101807177631480481363638366209', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-10 18:07:18', NULL, NULL);
INSERT INTO `wms_ware_order_task` VALUES (51, NULL, '202201101820512641480484775725711361', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-10 18:20:52', NULL, NULL);
INSERT INTO `wms_ware_order_task` VALUES (52, NULL, '202201110932193671480714154250608642', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-11 09:32:20', NULL, NULL);
INSERT INTO `wms_ware_order_task` VALUES (53, NULL, '202201111639538741480821757047799809', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-11 16:39:54', NULL, NULL);
INSERT INTO `wms_ware_order_task` VALUES (54, NULL, '202201111739409361480836802280439810', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-11 17:39:42', NULL, NULL);
INSERT INTO `wms_ware_order_task` VALUES (61, NULL, '202201111750052091480839420637626369', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-11 17:50:06', NULL, NULL);

-- ----------------------------
-- Table structure for wms_ware_order_task_detail
-- ----------------------------
DROP TABLE IF EXISTS `wms_ware_order_task_detail`;
CREATE TABLE `wms_ware_order_task_detail`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `sku_id` bigint(20) NULL DEFAULT NULL COMMENT 'sku_id',
  `sku_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'sku_name',
  `sku_num` int(11) NULL DEFAULT NULL COMMENT '购买个数',
  `task_id` bigint(20) NULL DEFAULT NULL COMMENT '工作单id',
  `ware_id` bigint(20) NULL DEFAULT NULL COMMENT '仓库id',
  `lock_status` int(1) NULL DEFAULT NULL COMMENT '1-已锁定  2-已解锁  3-扣减',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 68 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '库存工作单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wms_ware_order_task_detail
-- ----------------------------
INSERT INTO `wms_ware_order_task_detail` VALUES (19, 2, '', 1, 30, 3, 2);
INSERT INTO `wms_ware_order_task_detail` VALUES (20, 1, '', 1, 30, 2, 2);
INSERT INTO `wms_ware_order_task_detail` VALUES (21, 2, '', 1, 31, 3, 2);
INSERT INTO `wms_ware_order_task_detail` VALUES (22, 1, '', 1, 31, 2, 2);
INSERT INTO `wms_ware_order_task_detail` VALUES (23, 2, '', 1, 32, 3, 2);
INSERT INTO `wms_ware_order_task_detail` VALUES (24, 1, '', 1, 32, 2, 2);
INSERT INTO `wms_ware_order_task_detail` VALUES (25, 2, '', 1, 33, 3, 2);
INSERT INTO `wms_ware_order_task_detail` VALUES (26, 1, '', 1, 33, 2, 2);
INSERT INTO `wms_ware_order_task_detail` VALUES (27, 2, '', 1, 34, 3, 2);
INSERT INTO `wms_ware_order_task_detail` VALUES (28, 1, '', 1, 34, 2, 2);
INSERT INTO `wms_ware_order_task_detail` VALUES (29, 2, '', 1, 35, 3, 2);
INSERT INTO `wms_ware_order_task_detail` VALUES (30, 1, '', 1, 35, 2, 2);
INSERT INTO `wms_ware_order_task_detail` VALUES (31, 2, '', 1, 36, 3, 2);
INSERT INTO `wms_ware_order_task_detail` VALUES (32, 1, '', 1, 36, 2, 2);
INSERT INTO `wms_ware_order_task_detail` VALUES (33, 2, '', 1, 37, 3, 2);
INSERT INTO `wms_ware_order_task_detail` VALUES (34, 1, '', 1, 37, 2, 2);
INSERT INTO `wms_ware_order_task_detail` VALUES (35, 2, '', 1, 38, 3, 2);
INSERT INTO `wms_ware_order_task_detail` VALUES (36, 1, '', 1, 38, 2, 2);
INSERT INTO `wms_ware_order_task_detail` VALUES (37, 2, '', 1, 39, 3, 2);
INSERT INTO `wms_ware_order_task_detail` VALUES (38, 1, '', 1, 39, 2, 2);
INSERT INTO `wms_ware_order_task_detail` VALUES (39, 2, '', 1, 40, 3, 2);
INSERT INTO `wms_ware_order_task_detail` VALUES (40, 1, '', 1, 40, 2, 2);
INSERT INTO `wms_ware_order_task_detail` VALUES (41, 2, '', 1, 41, 3, 2);
INSERT INTO `wms_ware_order_task_detail` VALUES (42, 1, '', 1, 41, 2, 2);
INSERT INTO `wms_ware_order_task_detail` VALUES (43, 2, '', 1, 42, 3, 2);
INSERT INTO `wms_ware_order_task_detail` VALUES (44, 1, '', 1, 42, 2, 2);
INSERT INTO `wms_ware_order_task_detail` VALUES (45, 2, '', 1, 43, 3, 2);
INSERT INTO `wms_ware_order_task_detail` VALUES (46, 1, '', 1, 43, 2, 2);
INSERT INTO `wms_ware_order_task_detail` VALUES (47, 2, '', 1, 44, 3, 2);
INSERT INTO `wms_ware_order_task_detail` VALUES (48, 1, '', 1, 44, 2, 2);
INSERT INTO `wms_ware_order_task_detail` VALUES (49, 2, '', 1, 45, 3, 1);
INSERT INTO `wms_ware_order_task_detail` VALUES (50, 1, '', 1, 45, 2, 1);
INSERT INTO `wms_ware_order_task_detail` VALUES (51, 2, '', 1, 46, 3, 2);
INSERT INTO `wms_ware_order_task_detail` VALUES (52, 1, '', 1, 46, 2, 2);
INSERT INTO `wms_ware_order_task_detail` VALUES (55, 4, '', 1, 49, 3, 2);
INSERT INTO `wms_ware_order_task_detail` VALUES (56, 4, '', 1, 50, 3, 2);
INSERT INTO `wms_ware_order_task_detail` VALUES (57, 4, '', 1, 51, 3, 2);
INSERT INTO `wms_ware_order_task_detail` VALUES (58, 4, '', 1, 52, 3, 2);
INSERT INTO `wms_ware_order_task_detail` VALUES (59, 4, '', 1, 53, 3, 2);
INSERT INTO `wms_ware_order_task_detail` VALUES (60, 4, '', 1, 54, 3, 2);
INSERT INTO `wms_ware_order_task_detail` VALUES (67, 4, '', 1, 61, 3, 2);

-- ----------------------------
-- Table structure for wms_ware_sku
-- ----------------------------
DROP TABLE IF EXISTS `wms_ware_sku`;
CREATE TABLE `wms_ware_sku`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `sku_id` bigint(20) NULL DEFAULT NULL COMMENT 'sku_id',
  `ware_id` bigint(20) NULL DEFAULT NULL COMMENT '仓库id',
  `stock` int(11) NULL DEFAULT NULL COMMENT '库存数',
  `sku_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'sku_name',
  `stock_locked` int(11) NULL DEFAULT 0 COMMENT '锁定库存',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `sku_id`(`sku_id`) USING BTREE,
  INDEX `ware_id`(`ware_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '商品库存' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wms_ware_sku
-- ----------------------------
INSERT INTO `wms_ware_sku` VALUES (1, 1, 2, 100, '华为', 1);
INSERT INTO `wms_ware_sku` VALUES (2, 2, 3, 100, '小米', 1);
INSERT INTO `wms_ware_sku` VALUES (3, 4, 3, 100, '华为', 0);

SET FOREIGN_KEY_CHECKS = 1;
