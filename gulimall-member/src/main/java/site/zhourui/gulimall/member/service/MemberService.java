package site.zhourui.gulimall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import site.zhourui.common.utils.PageUtils;
import site.zhourui.gulimall.member.entity.MemberEntity;
import site.zhourui.gulimall.member.exception.PhoneException;
import site.zhourui.gulimall.member.exception.UsernameException;
import site.zhourui.gulimall.member.vo.GiteeSocialUser;
import site.zhourui.gulimall.member.vo.MemberUserLoginVo;
import site.zhourui.gulimall.member.vo.MemberUserRegisterVo;

import java.util.Map;

/**
 * 会员
 *
 * @author zr
 * @email 2437264464@qq.com
 * @date 2021-09-28 15:41:12
 */
public interface MemberService extends IService<MemberEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 用户注册
     * @param vo
     */
    void register(MemberUserRegisterVo vo);

    /**
     * 判断邮箱是否重复
     * @param phone
     * @return
     */
    void checkPhoneUnique(String phone) throws PhoneException;

    /**
     * 判断用户名是否重复
     * @param userName
     * @return
     */
    void checkUserNameUnique(String userName) throws UsernameException;

    /**
     * 用户登录
     */
    MemberEntity login(MemberUserLoginVo vo);
    /**
     * 社交用户的登录
     * @param socialUser
     * @return
     */
    MemberEntity login(GiteeSocialUser socialUser) throws Exception;


}

